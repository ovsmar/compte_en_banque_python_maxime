from datetime import datetime

class Compte:

    def __init__(self, id):
        self.id = id
        self.solde = 0.0
        self.da = 0.0
    
    def ajouter(self,valeur):
        self.solde += valeur
        return self.solde
    
    def retirer(self,valeur):
        if self.solde - valeur >= self.da:
            self.solde -= valeur
        return self.solde

    def getSolde(self):
        return self.solde

    def setDA(self, valeur):
        if valeur < 0:
            self.da = valeur
            return True
        else:
            return False
    
    def getCompte(self):
        return {
            "id": self.id,
            "solde": self.solde,
            "da": self.da,
            "timestamp": datetime.now()
        }

